odoo.define('pos_fixed_discount.DiscountButton', function(require) {
    'use strict';

    const PosComponent = require('point_of_sale.PosComponent');
    const ProductScreen = require('point_of_sale.ProductScreen');
    const { useListener } = require('web.custom_hooks');
    const Registries = require('point_of_sale.Registries');
    const DiscountButton = require('pos_discount.DiscountButton');

    const PosFixedDiscDiscountButton = (DiscountButton) =>
        class extends DiscountButton {
            async onClick() {
                var self = this;
                if (this.env.pos.user.groups_id.includes(
                    this.env.pos.config.group_discount_security[0]) === false) {
                    await this.showPopup('ErrorPopup', {
                        title : this.env._t("Unable to change Discount - Unauthorized"),
                        body  : this.env._t("Please ask your manager to do it."),
                    });
                    return;
                }
                super.onClick();
            }
         };

    Registries.Component.extend(DiscountButton, PosFixedDiscDiscountButton);

    return DiscountButton;
});

