# Copyright (C) 2016-Today: La Louve (<http://www.lalouve.net/>)
# @author: Sylvain LE GAL (https://twitter.com/legalsylvain)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from openerp import fields, models, api


class PosConfig(models.Model):
    _inherit = 'pos.config'

    group_discount_security = fields.Many2one(
        comodel_name='res.groups',
        compute='_compute_group_discount_security',
        string='Point of Sale - Discount Security',
        help="This field is there to pass the id of the 'PoS - Discount Security'"
        " Group to the Point of Sale Frontend.")

    def _compute_group_discount_security(self):
        for config in self:
            config.group_discount_security = \
                config.env.ref('mast_discount_security.group_discount_security')

    
