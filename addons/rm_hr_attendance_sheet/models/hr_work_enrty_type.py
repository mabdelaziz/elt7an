from odoo import api, fields, models
from datetime import date, timedelta

class HrAttendance(models.Model):

    _inherit = 'hr.attendance'

    checkin_date = fields.Date(compute='_compute_checkin_date')

    @api.depends('check_in')
    def _compute_checkin_date(self):
        for rec in self:
            rec.checkin_date = False
            if rec.check_in:
                rec.checkin_date = fields.Datetime.from_string(rec.check_in).date()

