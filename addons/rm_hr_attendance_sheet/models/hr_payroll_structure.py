# -*- coding: utf-8 -*-

from odoo import fields, models


class SalaryStructure(models.Model):
    _inherit = 'hr.payroll.structure'

    type_id = fields.Many2one(
        comodel_name='hr.payroll.structure.type',
        string='Structure Type')

    unpaid_work_entry_type_ids = fields.Many2many(
        comodel_name='hr.work.entry.type',
        string='Unpaid Work Entry Types')
