# -*- coding: utf-8 -*-

from odoo import fields, models


class SalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    struct_id = fields.Many2one(
        comodel_name='hr.payroll.structure',
        string='Payroll Structure')
