# -*- coding: utf-8 -*-

from odoo import fields, models


class SalaryStructureType(models.Model):
    _inherit = 'hr.payroll.structure.type'

    default_struct_id = fields.Many2one(
        comodel_name='hr.payroll.structure',
        string='Default Structure')

