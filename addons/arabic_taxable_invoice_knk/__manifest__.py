# -*- coding: utf-8 -*-
# Powered by Kanak Infosystems LLP.
# © 2020 Kanak Infosystems LLP. (<https://www.kanakinfosystems.com>).

{
    "name": "Arabic taxable invoice",
    "version": "1.0",
    "summary": "Arabic Taxable Invoice Module is the Invoice Receipt Layout which is printed in english as well as Arabic language to ensure customer's ease of readability and displays content in proper format ready to use for commerical purpose | Invoice Report | qweb report | Taxable Invoice | invoice report | araboc invoice report | Arabic Invoice | arabic invice",
    "description": """
        Arabic Taxable Invoice Module is the Invoice Receipt Layout which is printed in english as well as Arabic language to ensure customer's ease of readability and displays content in proper format ready to use for commerical purpose.
    """,
    "author": "Kanak Infosystems LLP.",
    "website": "http://www.kanakinfosystems.com",
    "category": "Accounting",
    "depends": ["account"],
    "data": [
        "data/report_paperformat.xml",
        "report/account_report.xml",
        "views/invoice_report.xml",
        "views/invoice_view.xml",
        "views/res_company.xml",
    ],
    'images': ['static/description/banner.jpg'],
    'license': 'OPL-1',
    'installable': True,
    'application': True,
    'auto_install': False,
}
