# -*- coding: utf-8 -*-
""" init object """
import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import fields, models, api, _, tools, SUPERUSER_ID
import logging

LOGGER = logging.getLogger(__name__)


class BatchAttendanceSheet(models.Model):
    _name = 'batch.attendance.sheet'

    
    def action_attsheet_confirm(self):
        self.write({'state': 'confirm'})
        for line in self.att_sheet_line_ids:
            line.write({'state': 'confirm'})
        return True

    
    def action_attsheet_approve(self):
        self.calculate_att_data()
        self.write({'state': 'done'})
        for line in self.att_sheet_line_ids:
            line.write({'state': 'done'})
        return True

    
    def action_attsheet_draft(self):
        self.write({'state': 'draft'})
        for line in self.att_sheet_line_ids:
            line.write({'state': 'draft'})
        return True

    name = fields.Char("name")
    date_from = fields.Date(string="From", required=True, default=time.strftime('%Y-%m-01'))
    date_to = fields.Date(string="To", required=True,
                          default=str(datetime.now() + relativedelta(months=+1, day=1, days=-1))[:10])
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed'),
        ('done', 'Approved')], default='draft', track_visibility='onchange',
        string='Status', required=True, readonly=True, index=True,
        help=' * The \'Draft\' status is used when a HR user is creating a new  attendance sheet. '
             '\n* The \'Confirmed\' status is used when  attendance sheet is confirmed by HR user.'
             '\n* The \'Approved\' status is used when  attendance sheet is accepted by the HR Manager.')
    # sheet_ids = fields.One2many('attendance.sheet', 'batch_id')

