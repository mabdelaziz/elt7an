# ?? 2015-2016 Akretion (http://www.akretion.com)
# @author Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).


{
    "name": "Fleet Vehicle Customization",
    "version": "14.0.1.0.1",
    "category": "fleet vehicle",
    "license": "AGPL-3",
    "author": "Mahmoud Abdelaziz",
    "depends": ["fleet"],
    "data": ["views/fleet_vehicle.xml"],
    "installable": True,
}
