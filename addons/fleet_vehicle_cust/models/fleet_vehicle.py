from odoo import fields, models, api, _
from odoo.exceptions import ValidationError


class FleetVehicle(models.Model):
    _inherit = "fleet.vehicle"

    motor_no = fields.Char('Motor Number')
    payload_capacity = fields.Char('Payload Capacity')
    fuel_capacity = fields.Integer('Fuel Capacity')
    license_no = fields.Char('License No.')
    issue_date = fields.Date('Issue Date')
    expiry_date = fields.Date('Expiry Date')

    @api.depends('license_no')
    def _onchange_license_no(self):
        for rec in self:
            if rec.license_no and not rec.issue_date and not rec.expiry_date:
                raise ValidationError(_("Issue Date &  Expiry Date Are required."))
            if rec.license_no and not rec.issue_date:
                raise ValidationError(_("Issue Date is required."))
            if rec.license_no and not rec.expiry_date:
                raise ValidationError(_("Expiry Date is required."))

    @api.onchange('issue_date', 'expiry_date')
    @api.depends('issue_date', 'expiry_date')
    def _validate_date(self):
        for rec in self:
            if rec.expiry_date and rec.issue_date:
                if rec.expiry_date <= rec.issue_date:
                    raise ValidationError(_("Issue Date Must be greeter than Expiry Date."))
