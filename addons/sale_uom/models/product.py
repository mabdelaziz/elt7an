from odoo import fields, models, api, _


class Product(models.Model):
    _inherit = 'product.template'

    sale_uom_ids = fields.Many2many('uom.uom', string='Sale UOM')
    uom_category_id = fields.Many2one('uom.category', related='uom_id.category_id',string='Uom Category')

