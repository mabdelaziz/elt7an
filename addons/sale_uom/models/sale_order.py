from odoo import fields, models, api, _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    sale_uom_ids = fields.Many2many('uom.uom', related='product_id.sale_uom_ids', string='Sale UOM')
