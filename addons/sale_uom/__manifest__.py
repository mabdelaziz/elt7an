# -*- coding: utf-8 -*-

{
    'name': 'Allowed Sales UOM',
    'version': '14.0.1.0.0',
    'category': 'tool',
    'summary': """
    Allowed Sales UOM
    """,
    'author': 'Mahmoud Abdelaziz',
    'depends': ['sale_management', 'sale', 'product'],
    'data': [
        'views/product_view.xml',
        'views/sale_order_view.xml',
    ],
    'installable': True,
    'license': 'AGPL-3',
}
