# -*- coding: utf-8 -*-

from odoo.exceptions import UserError, ValidationError
from odoo import models, fields, api, _


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    att_sheet_id = fields.Many2one('attendance.sheet', 'Attendance Sheet', readonly=True)
    att_policy_id = fields.Many2one(comodel_name='hr.attendance.policy', string="Attendance Policy ")
    total_late_hours = fields.Float('Total Late Hours', compute='_compute_total_hours')
    total_absence_hours = fields.Float('Total Absence Hours', compute='_compute_total_hours')
    total_leaves_hours = fields.Float('Total Leaves Hours', compute='_compute_total_hours')
    total_attended_hours = fields.Float('Total attended Hours', compute='_compute_total_hours')
    rejected_reason = fields.Text()

    def action_payslip_cancel(self):
        for rec in self:
            if not rec.rejected_reason:
                raise ValidationError(_('Please Add Rejected Reason.'))
        return super(HrPayslip, self).action_payslip_cancel()

    def action_payslip_done(self):
        for rec in self:
            if not rec.line_ids:
                raise UserError(_("Cannot Confirm Payslip With no Salary Rules."))
            return super(HrPayslip, self).action_payslip_done()

    
    @api.depends('worked_days_line_ids')
    def _compute_total_hours(self):
        for rec in self:
            rec.total_attended_hours = rec.worked_days_line_ids.filtered(lambda w: w.code == 'WORK100').number_of_hours
            rec.total_late_hours = rec.worked_days_line_ids.filtered(lambda w: w.code == 'LATE').number_of_hours
            rec.total_absence_hours = rec.worked_days_line_ids.filtered(lambda w: w.code == 'ABS').number_of_hours
            holidays = self.env['hr.leave'].sudo().search([
                ('employee_id', '=', rec.employee_id.id),
                ('date_from', '>=', rec.date_from),
                ('date_from', '<=', rec.date_to),
                ('state', '=', 'validate')
            ])
            rec.total_leaves_hours = False
            if rec.contract_id:
                hours_per_day = rec.contract_id.resource_calendar_id.hours_per_day
                rec.total_leaves_hours = sum(holidays.mapped('number_of_days')) * hours_per_day

    
    def get_attendance_sheet(self):
        """"""
        for rec in self:
            if not rec.att_policy_id:
                return
            # if three is old sheet delete it
            old_sheet = self.env['attendance.sheet'].search(
                [('employee_id', '=', rec.employee_id.id), ('att_policy_id', '=', rec.att_policy_id.id),
                 ('date_from', '=', rec.date_from),
                 ('date_to', '=', rec.date_from)])
            if old_sheet:
                old_sheet.sudo().unlink()
            sheet_name = _('Attendance Sheet of %s form %s to %s') % (
                rec.employee_id.name, fields.Date.to_string(rec.date_from), fields.Date.to_string(rec.date_to))
            sheet_id = self.env['attendance.sheet'].create({'employee_id': rec.employee_id.id,
                                                            'name': sheet_name,
                                                            'att_policy_id': rec.att_policy_id.id,
                                                            'date_from': rec.date_from,
                                                            'date_to': rec.date_to})
            sheet_id.get_attendances()
            sheet_id.action_attsheet_approve()
            rec.att_sheet_id = sheet_id
            rec.onchange_employee()

    @api.onchange('employee_id')
    def onchange_employee_policy_att_policy(self):
        """"""
        if self.contract_id.att_policy_id:
            self.att_policy_id = self.contract_id.att_policy_id

    def compute_sheet(self):
        for rec in self:
            rec.onchange_employee_policy_att_policy()
            if not rec.att_sheet_id:
                rec.get_attendance_sheet()
        return super(HrPayslip, self).compute_sheet()

    def get_worked_day_lines(self, contracts, date_from, date_to):
        """"""
        res = super(HrPayslip, self).get_worked_day_lines(contracts, date_from, date_to)
        if self.att_sheet_id:
            sheet_worked_days, absence_days, absence_hours = self.get_sheet_worked_days()
            if sheet_worked_days:
                res.extend(sheet_worked_days)

            res[0]['number_of_days'] = res[0]['number_of_days'] - absence_days
            res[0]['number_of_hours'] = res[0]['number_of_hours'] - absence_hours
        return res

    def get_sheet_worked_days(self):
        """"""
        sheet = self.att_sheet_id.read(['no_absence', 'tot_absence', 'no_late', 'tot_late'])
        worked_days_line_ids = []
        if not self.att_sheet_id:
            return worked_days_line_ids

        absence = [{
            'name': "Absence",
            'code': 'ABS',
            'contract_id': self.contract_id,
            'sequence': 35,
            'number_of_days': sheet[0]['no_absence'],
            'number_of_hours': sheet[0]['tot_absence'],
        }]
        late = [{
            'name': "Late In",
            'code': 'LATE',
            'contract_id': self.contract_id,
            'sequence': 40,
            'number_of_days': 0,
            'number_of_hours': sheet[0]['tot_late'],
        }]
        # overtime = [{
        #     'name': "Overtime",
        #     'code': 'OVT',
        #     'contract_id': self.contract_id,
        #     'sequence': 30,
        #     'number_of_days': self.att_sheet_id.no_overtime,
        #     'number_of_hours': self.att_sheet_id.tot_overtime,
        # }]
        # difftime = [{
        #     'name': "Difference time",
        #     'code': 'DIFFT',
        #     'contract_id': self.contract_id,
        #     'sequence': 45,
        #     'number_of_days': self.att_sheet_id.no_difftime,
        #     'number_of_hours': self.att_sheet_id.tot_difftime,
        # }]
        # worked_days_line_ids += overtime + late + absence + difftime
        worked_days_line_ids += late + absence
        if worked_days_line_ids:
            return worked_days_line_ids, sheet[0]['no_absence'], sheet[0]['tot_absence']

    
    def view_attendance_sheet(self):
        for rec in self:
            if not rec.att_sheet_id:
                raise ValidationError(_('There is no attendance Sheet'))

            return {
                'type': 'ir.actions.act_window',
                'res_model': 'attendance.sheet',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': rec.att_sheet_id.id,
                'views': [(False, 'form')],
            }
