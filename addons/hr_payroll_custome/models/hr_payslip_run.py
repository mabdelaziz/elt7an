# -*- coding: utf-8 -*-

from odoo.exceptions import UserError, ValidationError
from odoo import models, fields, api, _


class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'

    state = fields.Selection(selection_add=[('done', 'Done')])

    def action_batch_compute_sheet(self):
        for rec in self:
            if not rec.slip_ids:
                raise UserError(_("You Cannot Compute Sheet With No Slips."))
            for slip in rec.slip_ids:
                slip.compute_sheet()

    def action_confirm_payslips(self):
        for rec in self:
            if not rec.slip_ids:
                raise UserError(_("You Cannot Confirm With no Slips."))
            for slip in rec.slip_ids:
                slip.action_payslip_done()
            rec.state = 'done'

    def get_batch_attendance_sheet(self):
        """"""
        for rec in self:
            for slip in rec.slip_ids:
                slip.get_attendance_sheet()
