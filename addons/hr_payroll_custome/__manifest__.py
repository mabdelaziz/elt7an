# -*- coding: utf-8 -*-
{
    'name': 'HR Payroll Custom Updates',
    'version': '14.0.1.0.0',
    'summary': """Manages Payroll Updates""",
    'description': """""",
    'category': 'Generic Modules/Human Resources',
    'author': 'Mahmoud Abdelaziz',
    'depends': ['base', 'hr_payroll_community','hr_attendance_sheet'],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_payslip.xml',
        'views/hr_payslip_run.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
